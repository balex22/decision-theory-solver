package ru.spb.beavers.core.data;

import ru.spb.beavers.modules.ITaskModule;

import java.io.File;
import java.net.URL;
import java.util.LinkedList;

/**
 * Хранилище модулей
 */
public class StorageModules {

    private static final LinkedList<ITaskModule> MODULES = new LinkedList<>();
    private static ITaskModule activeModule;

    static {
        try {
            String packageName = ITaskModule.class.getPackage().getName().replace(".", "/");
            URL resource = ClassLoader.getSystemClassLoader().getResource(packageName);

            assert resource != null;
            File modulesDir = new File(resource.toURI());
            File[] moduleClasses = modulesDir.listFiles();

            assert moduleClasses != null;
            for (File moduleClass : moduleClasses) {
                String fileName = moduleClass.getName();
                if (moduleClass.isDirectory() || !fileName.endsWith(".class")) {
                    continue;
                }
                String className = fileName.substring(0, fileName.length() - ".class".length());
                Class<?> module = Class.forName(ITaskModule.class.getPackage().getName() + '.' + className);
                if (!module.isInterface() && ITaskModule.class.isAssignableFrom(module)) {
                    MODULES.add((ITaskModule) module.newInstance());
                }
            }
            if (MODULES.isEmpty()) {
                throw new RuntimeException("Не найдены модули для загрузки!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static LinkedList<ITaskModule> getModules() {
        return MODULES;
    }

    public static void setActiveModule(ITaskModule newActiveModule) {
        activeModule = newActiveModule ;
    }

    public static ITaskModule getActiveModule() {
        return activeModule;
    }
}
