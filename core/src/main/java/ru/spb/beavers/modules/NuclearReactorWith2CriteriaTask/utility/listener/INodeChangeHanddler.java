package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Node;

public interface INodeChangeHanddler {
    public void valueChanged(Node node);
}
